# tau Midi数字音乐合成器库(tau midi synthesizer lib)
#### 1.该库具备Midi音乐解析播放，soundfont解析播放，支持混音，合唱，压缩器，均衡器等音效添加。
#### 2.支持边缓存边播放模式
#### 3.支持对黑乐谱的流畅播放。
#### 4.支持C#播放midi音乐等
#### 5.增加了一个物理钢琴合成音源


### 支持系统平台
目前支持
   Window， 
   Android 平台
   
### 工程文件包括测试示例
TauClrTest(C#)
TauTest(C++)

### Visual Studio 编译
VS工程启动 直接运行编译

### Android Studio 编译
AS工程启动 直接运行编译


### 提供接口
tau的 C# 接口库和测试示例

